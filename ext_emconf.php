<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "hive_ext_bodycompass"
 *
 * Auto generated by Extension Builder 2017-07-31
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'hive_ext_bodycompass',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Krüger, Josymar Escalona Rodriguez, Perrin Ennen, Timo Bittner',
    'author_email' => 'a.hafner@teufels.com, d.hilser@teufels.com, g.kathan@teufels.com, h.krueger@teufels.com, j.rodriguez@teufels.com, p.ennen@teufels.com, t.bittner@teufels.com',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '8.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.6.0-0.0.0',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
