<?php
namespace HIVE\HiveExtBodycompass\Controller;

/***
 *
 * This file is part of the "hive_ext_bodycompass" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * EntryController
 */
class EntryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * entryRepository
     *
     * @var \HIVE\HiveExtBodycompass\Domain\Repository\EntryRepository
     * @inject
     */
    protected $entryRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $entries = $this->entryRepository->findAll();
        $this->view->assign('entries', $entries);
    }
}
