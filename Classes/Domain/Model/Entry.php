<?php
namespace HIVE\HiveExtBodycompass\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_bodycompass" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Entry
 */
class Entry extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * backendtitle
     *
     * @var string
     * @validate NotEmpty
     */
    protected $backendtitle = '';

    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * indications
     *
     * @var string
     */
    protected $indications = '';

    /**
     * Returns the backendtitle
     *
     * @return string $backendtitle
     */
    public function getBackendtitle()
    {
        return $this->backendtitle;
    }

    /**
     * Sets the backendtitle
     *
     * @param string $backendtitle
     * @return void
     */
    public function setBackendtitle($backendtitle)
    {
        $this->backendtitle = $backendtitle;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the indications
     *
     * @return string $indications
     */
    public function getIndications()
    {
        return $this->indications;
    }

    /**
     * Sets the indications
     *
     * @param string $indications
     * @return void
     */
    public function setIndications($indications)
    {
        $this->indications = $indications;
    }
}
