<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtBodycompass',
            'Hivebodycompasslistbodycompass',
            [
                'Entry' => 'list'
            ],
            // non-cacheable actions
            [
                'Entry' => 'list'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivebodycompasslistbodycompass {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_bodycompass') . 'Resources/Public/Icons/user_plugin_hivebodycompasslistbodycompass.svg
                        title = LLL:EXT:hive_ext_bodycompass/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_bodycompass_domain_model_hivebodycompasslistbodycompass
                        description = LLL:EXT:hive_ext_bodycompass/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_bodycompass_domain_model_hivebodycompasslistbodycompass.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextbodycompass_hivebodycompasslistbodycompass
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder