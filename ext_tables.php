<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtBodycompass',
            'Hivebodycompasslistbodycompass',
            'hive :: BodyCompass :: listBodyCompass'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_bodycompass', 'Configuration/TypoScript', 'hive_ext_bodycompass');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextbodycompass_domain_model_entry', 'EXT:hive_ext_bodycompass/Resources/Private/Language/locallang_csh_tx_hiveextbodycompass_domain_model_entry.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextbodycompass_domain_model_entry');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextbodycompass_domain_model_description', 'EXT:hive_ext_bodycompass/Resources/Private/Language/locallang_csh_tx_hiveextbodycompass_domain_model_description.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextbodycompass_domain_model_description');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder