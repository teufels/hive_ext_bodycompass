
plugin.tx_hiveextbodycompass_hivebodycompasslistbodycompass {
    view {
        # cat=plugin.tx_hiveextbodycompass_hivebodycompasslistbodycompass/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_bodycompass/Resources/Private/Templates/
        # cat=plugin.tx_hiveextbodycompass_hivebodycompasslistbodycompass/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_bodycompass/Resources/Private/Partials/
        # cat=plugin.tx_hiveextbodycompass_hivebodycompasslistbodycompass/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_bodycompass/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextbodycompass_hivebodycompasslistbodycompass//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder